#!/bin/bash
ssh_key_file="/vagrant/.ssh/id_rsa-remotessh.pub"
# Проверяем, является ли ОС Linux на базе RedHat
if [ -f /etc/redhat-release ]; then
		sudo su -
		if [ -f "$ssh_key_file" ]; then
			cat "$ssh_key_file" >> /home/vagrant/.ssh/authorized_keys
		else
			echo "File '$ssh_key_file' not found!, ssh-key not apply"
		fi
		setenforce 0
		sed -i 's/^SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
		systemctl disable firewalld.service
		systemctl stop firewalld.service	
		#firewall-cmd --zone=public --add-port=80/tcp --permanent
		#firewall-cmd --zone=public --add-port=8080/tcp --permanent
		#firewall-cmd --reload
	  dnf update -y
    dnf install -y nano curl net-tools podman podman-docker docker-compose
		#curl -fsSL https://get.docker.com -o install-docker.sh
		#sh install-docker.sh
		#usermod -aG docker vagrant
		#systemctl enable --now docker.service docker.socket containerd.service

		# dnf clean all

		#systemctl daemon-reload
else
    echo "Пропущен запуск redhat-dev01.sh -> Скрипт предназначен для работы на ОС redhat-like"
    exit 0
fi
